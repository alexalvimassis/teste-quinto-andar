const css = require('../sass/wrapper.scss')

const field = document.querySelector('[data-validate]');

field.addEventListener("keyup", function(evt){
  let numbers = field.value.replace(/\D/g,"");
  if( numbers.length < 3 ){
    field.value = '(' + numbers;
  }else if(numbers.length < 7){
    field.value = '(' + numbers.substr(0, 2) + ') ' + numbers.substr(2, numbers.length -1 );
  }else if(numbers.length < 11) {
    field.value = '(' + numbers.substr(0, 2) + ') ' + numbers.substr(2, 4) + '-' + numbers.substr(6, numbers.length -1);
  }else if(numbers.length == 11) {
    field.value = '(' + numbers.substr(0, 2) + ') ' + numbers.substr(2, 5) + '-' + numbers.substr(7, 10);
    activeButton();
    field.blur();
  } else {
    field.value = '(' + numbers.substr(0, 2) + ') ' + numbers.substr(2, 5) + '-' + numbers.substr(7, 10);
    field.value = field.value.substr(0, 15);
    activeButton();
    field.blur();
  }
});

field.addEventListener("click", function(evt){
  field.value = ""
  document.querySelector('[data-button]').classList.remove('outlined-dark');
  document.querySelector('[data-button]').classList.add('disabled-button');
});

function activeButton() {
  document.querySelector('[data-button]').classList.remove('disabled-button');
  document.querySelector('[data-button]').classList.add('outlined-dark');
}
